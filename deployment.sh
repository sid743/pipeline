echo -e $PWD

echo -e '\e[1m\e[34mEntering into backend directory...\e[0m\n'

cd /root

echo -e $PWD

echo -e '\e[1m\e[34mPulling code from remote...\e[0m\n'

git pull origin master

echo -e '\e[1m\e[34mAll done now!\e[0m\n'

echo -e '\e[1m\e[34mThanks to Ileriayo for automating this deployment process!\e[0m\n'